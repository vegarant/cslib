% This file set variuous parameters to get consistency between files
% The parameters found in this file, will be considered as default parameters

clear('all'); close('all');

% Create destination for the data
dest = fullfile('..', 'var');
if (exist(dest) ~= 7) 
    mkdir(dest);
end

% cslib defaults
csl_dflt.font_size = 14; 
csl_dflt.marker_size = 8;
csl_dflt.color =[0,    0.4470,    0.7410]; % Blue 
csl_dflt.marker = 'o';
csl_dflt.marker_edge_color = [0,0,0]; % Black
csl_dflt.marker_face_color = [0,0,0]; % Black
csl_dflt.line_width = 2;
csl_dflt.image_format = 'png';
csl_dflt.plot_format = 'epsc';
csl_dflt.blue    = [0,    0.4470,    0.7410];
csl_dflt.yellow  = [1,1,0]; 
csl_dflt.green   = [102, 255, 51]./255;
csl_dflt.brown   = [153, 102, 51]./255;
csl_dflt.red     = [1, 0, 0];
csl_dflt.magenta = [1, 0, 1];
csl_dflt.cyan    = [0, 1, 1]; 
csl_dflt.black   = [0, 0, 0];

csl_dflt.line_color = 'k';
csl_dflt.spgl1_verbosity = 1;
csl_dflt.data_path = '/home/vegant/db-cs/cslib_data';

csl_dflt.cmap_matrix = jet(256);

save(fullfile(dest,'cslib_defaults.mat'));


