% Plots the activaction functions

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

% Create destination for the plots
dest = 'plots';
if (exist(dest) ~= 7) 
    mkdir(dest);
end
disp_plots = 'off';

M = 5;
xtic = [-M,-2.5,0,2.5,M];
t = linspace(-M,M,101);


fig = figure('visible', disp_plots);
plot(t, t.*(t>0), 'linewidth', csl_dflt.line_width, 'color', csl_dflt.black);
xticks(xtic);
set(gca, 'FontSize', csl_dflt.font_size+5);
set(gca,'LooseInset',get(gca,'TightInset'));
saveas(fig,fullfile(dest, 'act_relu'), csl_dflt.plot_format);


fig = figure('visible', disp_plots);
plot(t, 1./(1+exp(-t)), 'linewidth', csl_dflt.line_width, 'color', csl_dflt.black);
xticks(xtic);
set(gca, 'FontSize', csl_dflt.font_size+5);
set(gca,'LooseInset',get(gca,'TightInset'));
saveas(fig,fullfile(dest, 'act_sigmoid'), csl_dflt.plot_format);


fig = figure('visible', disp_plots);
plot(t, tanh(t), 'linewidth', csl_dflt.line_width, 'color', csl_dflt.black);
xticks(xtic);
set(gca, 'FontSize', csl_dflt.font_size+5);
set(gca,'LooseInset',get(gca,'TightInset'));
saveas(fig,fullfile(dest, 'act_tanh'), csl_dflt.plot_format);


fig = figure('visible', disp_plots);
plot(t, t.*(t>0) + 0.2.*t.*(t<0), 'linewidth', csl_dflt.line_width, 'color', csl_dflt.black);
xticks(xtic);
set(gca, 'FontSize', csl_dflt.font_size+5);
set(gca,'LooseInset',get(gca,'TightInset'));
saveas(fig,fullfile(dest, 'act_lrelu'), csl_dflt.plot_format);



