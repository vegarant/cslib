clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

% Create destination for the plots
dest = 'plots/';
if (exist(dest) ~= 7) 
    mkdir(dest);
end

disp_plots = 'off';
dwtmode('per', 'nodisp');

th = 1;
S = @(z) sign(z).*(abs(z) - th).*(abs(z) >= th); 

bd = 2;

t = linspace(-bd,bd,101);

fig = figure('visible', disp_plots);


plot(t,S(t), csl_dflt.line_color,...
         'LineWidth', csl_dflt.line_width)

axis([-bd, bd, -bd+th,bd-th]);

set(gca, 'FontSize', csl_dflt.font_size);

fname = sprintf('soft_thresholding_tau_%d', th);
saveas(fig, fullfile(dest, fname), csl_dflt.plot_format);
