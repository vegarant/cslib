clear('all'); close('all');
dwtmode('per', 'nodisp');

r = 8;
N = 2^r;
subsampling_ratio = 0.05;
nbr_samples = round(subsampling_ratio*N*N);

vm = 4;
wname = sprintf('db%d', vm);
nres = wmaxlev(N, wname);
nres
im = phantom(N);
nres = wmaxlev(N, wname);
epsilon = .3;
sparsities = csl_compute_sparsity_of_image(im, vm, nres, epsilon);
sparsities
[idx, ~] = spf2_DIS(N, nbr_samples, sparsities, vm);
Z = zeros([N,N]);
Z(idx) = 1;

imagesc(Z); colormap('gray');


%N = 8;
%
%c = [N/2,N/2];
%radius = 1;
%p_norm = inf;
%shapef = csl_shape_ball(c, radius, p_norm);

