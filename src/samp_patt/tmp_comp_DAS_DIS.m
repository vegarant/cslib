clear('all'); close('all');
dwtmode('per', 'nodisp');

r = 10;
N = 2^r;
subsampling_ratio = 0.25;
nbr_samples = round(subsampling_ratio*N*N);

vm = 4;
wname = sprintf('db%d', vm);
nres = wmaxlev(N, wname);

im = phantom(N);
sValues    = csl_calculate_sparsities(im, nres, vm);
sparsities = csl_compute_sparsity_of_image(im, vm, nres);

nbr_levels = 10;
r0 = 1;
a = 2;
%[idx1, str_id] = spf2_gsquare(N, nbr_samples, a, r0, nbr_levels);
%[idx2, str_id] = spf2_gcircle(N, nbr_samples, a, r0, nbr_levels);
%[idx3, str_id] = sph2_gcircle(N, nbr_samples, a, r0, nbr_levels);
%[idx4, str_id] = sph2_gsquare(N, nbr_samples, a, r0, nbr_levels);


[idx1, str_id] = sph2_DIS(N, nbr_samples, sValues);
[idx2, str_id] = sph2_DAS(N, nbr_samples, sparsities);
[idx3, str_id] = sph2_DAS_randy(N, nbr_samples, sValues);
[idx4, str_id] = spf2_DIS(N, nbr_samples, sValues, vm);
[idx5, str_id] = spf2_DAS(N, nbr_samples, sparsities, vm);
[idx6, str_id] = spf2_DAS_randy(N, nbr_samples, sValues, vm);


Z1 = zeros([N,N]);
Z1(idx1) = 1;
Z2 = zeros([N,N]);
Z2(idx2) = 1;
Z3 = zeros([N,N]);
Z3(idx3) = 1;
Z4 = zeros([N,N]);
Z4(idx4) = 1;
Z5 = zeros([N,N]);
Z5(idx5) = 1;
Z6 = zeros([N,N]);
Z6(idx6) = 1;

title1 = sprintf('a: %g, r0: %d, levels: %d', a, r0, nbr_levels);

figure();
subplot(231); imagesc(Z1); colormap('gray'); title('DIS'); 
subplot(232); imagesc(Z2); colormap('gray'); title('DAS'); 
subplot(233); imagesc(Z3); colormap('gray'); title('DAS Randy'); 
subplot(234); imagesc(Z4); colormap('gray'); title('DIS'); 
subplot(235); imagesc(Z5); colormap('gray'); title('DAS'); 
subplot(236); imagesc(Z6); colormap('gray'); title('DAS Randy'); 



