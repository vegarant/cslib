% Function used to determine whether turboAMP is installed and to add the java
% files from turboAMP/hmt/hmt_structm to the java path. If turboAMP is not
% installed, a message will be written to the screen. 
%
% INPUT - None
%
% OUTPUT
% b - boolean saying whether turboAMP is installed
%
% Edvard Aksnes, 2017
function b=addpath_hmt_struct()
  str = which('getStructHMT');
  % if which does not find the specified method we assume TurboAMP is not installed 
  if isempty(str)
    display('Could not find an installation of turboAMP.')
    b=false;
  else
    str = strcat(str(1:end-14),'hmt_struct');
    javaaddpath(str)
    b=true;
  end
